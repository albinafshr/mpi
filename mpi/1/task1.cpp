#include <iostream>
#include <mpi.h>
using namespace std;

/* Написать mpi программу, печатающую "Hello, World!". Запустить программу на 4 процессах. */

int main()
{
    MPI_Init(NULL, NULL);

    cout << ("Hello world!") << endl;

    MPI_Finalize();
}