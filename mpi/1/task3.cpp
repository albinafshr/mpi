#include <mpi.h>
using namespace std;

/* Написать программу, запустить ее на 2х процессах. На нулевом процессе задать массив а из 10 элементов, 
значения сгенерировать случайным образом. Переслать этот массив целиком первому процессу с помощью функции 
MPI_Send. Распечатать на первом процессе принятый массив. */

int main()
{
	MPI_Init(NULL, NULL);

	int current_process_id;
    MPI_Comm_rank(MPI_COMM_WORLD, &current_process_id);

    int number_of_processes;
    MPI_Comm_size(MPI_COMM_WORLD, &number_of_processes);
	
	int a[10];
	
	if (current_process_id == 0)
	{
		cout << "Process " << current_process_id << " - array a generated: {";
		
		for (int i = 0; i < 10; i++)
		{
			a[i] = rand() % 100;
			cout << a[i];

			if (i < 9)
				cout << ", ";
			else
				cout << "}" << endl;
		}

		MPI_Send(&a, 10, MPI_INT, 1, 0, MPI_COMM_WORLD);
	}
	else if (current_process_id == 1)
	{
		cout << "Process " << current_process_id << " - array a received: {";

		MPI_Recv(&a, 10, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

		for (int i = 0; i < 10; i++)
		{
			cout << a[i];

			if (i < 9)
				cout << ", ";
			else
				cout << "}" << endl;
		}
	}
	
	MPI_Finalize();
}