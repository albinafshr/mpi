#include "mpi.h"
#include <math.h>
using namespace std;

/* Написать программу вычисления нормы матрицы А по формуле s=j=0..m(i=0nabs(a[i][j])), где m- число столбцов, 
n - число строк. Для распределения элементов матрицы А по процессам использовать функцию MPI_Scatter. Для 
получения итогового значения использовать функцию MPI_Reduce с операцией MPI_MAX. */

int main() 
{
	MPI_Init(NULL, NULL);

	int current_process_id;
    MPI_Comm_rank(MPI_COMM_WORLD, &current_process_id);

    int number_of_processes;
    MPI_Comm_size(MPI_COMM_WORLD, &number_of_processes);

    const int N = number_of_processes;
    const int M = 5;
    int a[N][M];
    int a_local[M];

    if (current_process_id == 0)
    {
        cout << "Generated numbers:" << endl;

        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < M; j++)
            {
                a[i][j] = rand() % 100 * (j % 2 ? -1 : 1);

                cout << a[i][j] << " " << endl;
            }
        }
    }

    MPI_Scatter(a, M, MPI_INT, &a_local, M, MPI_INT, 0, MPI_COMM_WORLD);

    int sum = 0;

    for (int i = 0; i < M; i++)
        sum += abs(a_local[i]);

    int final_result;

    MPI_Reduce(&sum, &final_result, N, MPI_INT, MPI_MAX, 0, MPI_COMM_WORLD);

    if (current_process_id == 0) 
        cout << "Final result: " << final_result << endl;

    MPI_Finalize();
}