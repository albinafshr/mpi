#include "mpi.h"
#include <math.h>
using namespace std;

/* Написать программу вычисления нормы ||x||1=i=0n-1abs(xi)вектора x. Для распределения элементов вектора x 
по процессам использовать функцию MPI_Scatter. Для получения итоговой суммы на нулевом процессе использовать 
функцию MPI_Reduce с операцией MPI_Sum. */

int main()
{
	MPI_Init(NULL, NULL);

	int current_process_id;
    MPI_Comm_rank(MPI_COMM_WORLD, &current_process_id);

    int number_of_processes;
    MPI_Comm_size(MPI_COMM_WORLD, &number_of_processes);

    const int N = number_of_processes;
    int x[N];
    int result;

    if (current_process_id == 0)
    {
        cout << "Generated numbers:" << endl;

        for (int i = 0; i < N; i++)
        {
            if (i % 2 == 0)
                x[i] = rand() % 10;
            else
                x[i] = rand() % 10 * (-1);

            cout << x[i] << endl;
        }
    }

    MPI_Scatter(x, 1, MPI_INT, &result, 1, MPI_INT, 0, MPI_COMM_WORLD);

    result = abs(result);

    cout << "Absolute: " << result << endl;

    int final_result;

    MPI_Reduce(&result, &final_result, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

    if (current_process_id == 0)
        cout << "Final result: " << final_result << endl;

    MPI_Finalize();
}