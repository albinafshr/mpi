#include "mpi.h"
#include <math.h>
using namespace std;

/* Написать программу вычисления скалярного произведения двух векторов(x,y)=i=0n-1xiyi. Для распределения 
элементов вектора x по процессам использовать функцию MPI_Scatter. Для получения итоговой суммы на нулевом 
процессе использовать функцию MPI_Reduce с операцией MPI_Sum. */

int main()
{
	MPI_Init(NULL, NULL);

	int current_process_id;
    MPI_Comm_rank(MPI_COMM_WORLD, &current_process_id);

    int number_of_processes;
    MPI_Comm_size(MPI_COMM_WORLD, &number_of_processes);

    const int N = number_of_processes;
    int x[N];
    int y[N];
    int x_local;
    int y_local;
    int result;

    if (current_process_id == 0)
    {
        cout << "Generated numbers:" << endl;

        for (int i = 0; i < N; i++)
        {
            x[i] = rand() % 100;
            y[i] = rand() % 100;

            cout << x[i] << " " << y[i] << endl;
        }
    }

    MPI_Scatter(x, 1, MPI_INT, &x_local, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Scatter(y, 1, MPI_INT, &y_local, 1, MPI_INT, 0, MPI_COMM_WORLD);

    result = x_local * y_local;

    cout << "Mult: " << result << endl;

    int final_result;

    MPI_Reduce(&result, &final_result, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

    if (current_process_id == 0)
        cout << "Final result: " << final_result << endl;

    MPI_Finalize();
}